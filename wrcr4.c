#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>

#define VME         0 /* Virtual 8086 Mode Extensions */
#define PVI         1 /* Protected-mode */
#define TSD         2 /* Time */
#define DE          3 /* Debugging */
#define PSE         4 /* Page */
#define PAE         5 /* Physical */
#define MCE         6 /* Machine */
#define PGE         7 /* Page */
#define PCE         8 /* Performance-Monitoring */
#define OSFXSR      9 /* Operating */
#define OSXMMEXCPT 10 /* Operating */
#define UMIP       11 /* User-Mode */
#define VMXE       13 /* Virtual */
#define SMXE       14 /* Safer */
#define FSGSBASE   16 /* Enables */
#define PCIDE      17 /* PCID */
#define OSXSAVE    18 /* XSAVE */
#define SMEP       20 /* Supervisor */
#define SMAP       21 /* Supervisor */
#define PKE        22 /* Protection */
#define CET        23 /* Control-flow */
#define PKS        24 /* Enable */

/* Set bit of clear bit */
#define CLEAR       0
#define SET         1

/* Return codes */
#define SUCCESS     0
#define FAILURE     1


/* Read cr4 register */
static uint64_t
rdcr4(void)
{
    uint64_t cr4 = 0;

    __asm__("mov %%cr4,%0\n" : "=r" (cr4));	

    return cr4;
}

/* Write cr4 register */
static void
wrcr4(uint64_t new_cr4)
{
    __asm__("mov %0, %%cr4\n" : : "r" (new_cr4));	 
}

/* Enable CET in cr4  */
static void
setcet(void *info)
{
    uint64_t cr4 = rdcr4();

    cr4 |= (1 << CET);
    wrcr4(cr4);
}

/* Clear CET in cr4 */
static void
clearcet(void *info)
{
    uint64_t cr4 = rdcr4();

    cr4 &= ~(1 << CET);
    wrcr4(cr4);
}

static void
testcet(void *info)
{
    uint64_t cr4 = rdcr4();

    printk(KERN_INFO "[wrcr4] CET %s\n",
            (((cr4 >> CET) & 1) ? "ENABLED" : "DISABLED"));
}

/* Initialize module  */
static int
init_func(void)
{
    printk(KERN_INFO "[wrcr4] initializing module\n");

    on_each_cpu(clearcet, NULL, 0);
    on_each_cpu(testcet, NULL, 0);

    return SUCCESS;
}

/* Cleanup module */
static void
exit_func(void)
{
    printk(KERN_INFO "[wrcr4] unloading module\n");

    on_each_cpu(setcet, NULL, 0);
    on_each_cpu(testcet, NULL, 0);
}

/* Register module load/unload callbacks */
module_init(init_func);
module_exit(exit_func);

/* Module information */
MODULE_AUTHOR("Alexander J. Gaidis");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Write cr4 to disable CET");
